

Api para repositorio de recibos, sube el archivo y lo pfrece via apidsfdasdadsas,

## Contenido

* [Introduccion](#introduccion)
* [Como usar este proyecto](#como-usar-este-proyecto)
    * [Despliegue y desarrollo](#despliegue-y-desarrollo)
    * [Infraestructura del proyecto](#infraestructura-del-proyecto)

## Introduccion

Sistema laravel tipo repositorio para almacenaje de documentos 
escaneados ( [recibos](docs/README-artifacts.md#artefactos) ).

Este sistema es parte de otros sistemas mas grandes, y punto de 
partida para el sistema de gastos y retenciones.

## Como usar este proyecto

Este repositorio concentra y sincroniza empleando **git-modules**,
y utilia el flujo de trabajo de ramas simplificado **GitFlow simplified**

Para clonarlo y trabajar en el, **DEBE leer [docs/README.md](docs/README.md)**

#### Despliegue y desarrollo

Los detalles para despliegue estan en [docs/README-produccion.md](docs/README-produccion.md), y para 
instalacion y desarrollo local el documento [docs/README.md](docs/README.md)

#### Infraestructura del proyecto

El proyecto esta basado en uan plantilla de api, con todo ya listo, 
y no debe salirse de lo que ya esta estructurado, puesto solo 
es subir archivos y ver archivos, puros get y post y una autenticacion simple.

Para mas detalles leer [docs/README.md](docs/README.md)

## LICENCIA

**CC-BY-SA-NC** Compartir igual sin derecho comercial a menos que se pida permiso, y con atribuciones de credito de lso creadores.

* (c) 2023 PICCORO Lenz McKAY <mckaygerhard>
* (c) 2023 xxxxxx xxxx
