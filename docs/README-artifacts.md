# artefactos de tecnologia

## actores

Realizan acciones para que se cumpla un proceso:

#### COMPRADOR

Es un usuario, El que va adquirir el [articulo(S)](#articulo) o **rif del agente (de retencion)**, 
es el que entrega dinero a el `VENDEDOR`:

#### VENDEDOR

Es un usuario, El que ofrece el bien a comprar o **rif del sujeto (retenido)**, 
este recibe el dinero y entrega el [articulo(S)](#articulo) al `COMPRADOR`.

#### CONTABILIDAD

ES un usuario o tambien una entidad o un sistema no usuario, 
Es o son quien(es) emite(n) y procesa(n) la informacion solo de [facturas](#factura) 
para asi procesar el ISLR o IVA, ya que debe contabilizar el [recibo de tipo factura](#factura) 
unicamente, para realizar retenciones y declaraciones al fisco.

#### GASTOS

ES un usuario o tambien una entidad o un sistema no usuario, 
Es o son quien(es) emite(n) y procesa(n) la informacion de [recibos](#recibo) 
tanto [facturas](#factura) como [notas](#notas), estos no procesan informacion, solo 
la registran para contabilizar costes e inversion.

## artefactos involucrasos

#### PRODUCTO

El el bien recibido por el **rif del agente** o `VENDEDOR` 
ofrecido por el **rif del sujeto** o `VENDEDOR`
hay de dos tipos:

###### articulos

Que puede ser computadores, escritorios, comida sin cocer, estos
**IMPORTANTE** estos generan IVA de uno o varios tipos.

###### servicios

Que puede ser prestados consumidos (comida cocida servida, internet, agua)
**IMPORTANTE** estos generan ISLR y deben ser declarados

#### RECIBO

Documento legal que acredita la recepción de productos/mercancías 
o dinero, las hay de varios tipos aqui siendo las mas importantes:

###### factura

Recibo, pero cuando tiene legalidad pero a nivel fiscal

###### notas

De entrega o de credito o de pagos, no tienen validez sino entre las partes

#### SISTEMA

Sonlso distintos artefactos de automatizacion de informacion, 
en la red y estaninvolucrados varios

* repositorio de recibos
* sistema de retenciones
* sistema de gastos
* interfaz de recibos (subidas, y administracion)

#### SERVIDOR

Es el lugar donde residen estos sistemas y por medio el cual 
los actores (de los cuales pueden ser usuarios o no) participan 
e interactuan con la informacion y los adjuntos o [recibos](#recibos) .

## proceso de repositorio recibos

El sistema es solo un API es decir ningun actor interactua 
directamente conel, pero este participa en los proceso, 
asi que se describe estos y la participacion de los actores:

1. COMPRADOR : compra con el rif, una nevera, una comida, paga el internet, como ejemplos
2. COMPRADOR : noitificar que se es contribuyente especial al vendedor
    * aeriguar que pasa si el que compra no lo dice
    * vendedor no esta en el deber de indicarle que retenga, el comprador debe hacerlo

#### Etapa 1 desarrollo

Esta etapa solo fabria el sistema de repositorio de recibos, 
y su funcionalidad se limitara a subir y mostrar recibos, 
con una interfaz embebida minima sin administracion:

###### proceso etapa 2 desarrollo

El sistema de resibos solo recive una peticion POST con el 
fomulario multiform los datos aqui y el adjunto.

* 3 - COMPRADOR : inmediatamente cargar la factura al sistema sea por telefono o por pc, y llenar los datos
    * rif del agente de retencion o el rif del que compra o el que hace la compra
    * rif del sujeto retenido o rif de la razon social a la que compran (el rif del negocio al que hicieron la compra)
    * numero de factura OJO no es lo mismo que numero de control
    * numero de control: OJO si es ticket o no tiene usar el numero de la caja, o el numero al final en la linea MH
    * fecha de la factura (no es lo mismo que fecha de entrega de factura ni fecha de realizacion de dicha compra)
    * fecha de la compra (ojo puede no ser la misma de la fecha de la factura), fecha de comra = fecha que se concreta o entrega dinero, el sistem automatico pone la del dia y el tipo la modifica hacia adelante
    * base imponible (el monto de la compra, pero solo los que se les saca iva pero sin el iva, o el monto cantidad de objeto de retencion)
    * base excento (el monto de la compra pero solo los que no se les aplica iva, total, o el monto si derecho a credito fiscal)
    * monto del iva (el iva correspondinte, ojo no simepre es 16% puede variar, es solo en bolivares asi en el pago esten dolares incluidos, se les saca convertido en bolivares, llamado impuesto iva)
    * porcentaje iva (este puede variasr, si es productos de transporte no es 16 por ejemplo)
    * monto total (es el del imponible + excento+ mas el del iva , es el total de compras)
    * **flag o selector** si es salida de dinero para pagar un **servicio** (pagos) o si es salida de dinero para **articulo** adquiridos (compra)

#### ETAPA 2 desarrollo

Esta etapa agrega un sistema de cola, para llevar control de el 
adjunto procesado, se limitara a marcar la cola en tres estados:

###### estado pendiente

Marca enla cola que el adjunto es nuevoy aun nadie sabe de el, 
segun el tipo de adjunto y naturaleza, envia notificaciones.

###### estado leido

Marca eladjunto como que ya fue obtenido del api por un sistema 
remoto (halo el adjunto por el gasto o retencion) y el usuario 
realizao algo con el mismo.

###### estado procesado

Marca que el adjunto ya esta en el otro sistema (ejemplo gasto, retencion)
y se saca de la cola.

###### proceso etapa 2 desarrollo

* 4 - SISTEMA: se coloca en la cola de la lista de procesos pendientes de retencion y avisos de gastos **todo salida de dinero genera un recibo**
    * SISTEMA: poner disponible el recibo cargado, por medio del api
    * SISTEMA: colocar en cola de el proceso, 
    * SISTEMA: enviar notificacion para gastos, siempre que se suba un recibo
    * SISTEMA: clasificarlo, entre esas clasificaciones estan la mas importante: servicio(pago) o articulo(compra), y contribuyente especial o no
    * si es contribuyente especial
        * SISTEMA: enviar notificacion para contabilidad siempre que sea un recibo tipo factura y sea contribuyente especial
        * si es un **servicio**
            * SISTEMA: colocar en otra cola solo para contabilidad, de el proceso, de retenciones para ISLR
        * si es un **articulo**
            * SISTEMA: colocar en otra cola solo para contabilidad, de el proceso, de retenciones para IVA

## Casos de uso

Estos determinan el desarrollo

https://gitlab.com/tijerazo/receiptsapi/-/boards/7072121

Las casos abiertos son los logros o objetivos del desarrollo 
pendientes por cumplir, los WIP son los que se estan trabajando.

Los casos de uso solo tienen la etiqueta **"Metas/Propuestas"** .




